/** 
 * A program that solves sudokus
 * Read sudoku rows via standard input
 */


// 1.0 Copy sudoku into an array
let input = process.argv.slice(2);
console.log("\n1.0 INPUT:");
console.table(input);


// 1.1 replace '.' with 0
let replaceDot = /\./g;
let sudoku1D = input.map(item => item.replace(replaceDot, 0));
console.log("\n1.1 DOTS TO ZEROS:\n");
console.table(sudoku1D);


// 1.2 validate the sudoku rows:
//      >> contains only numbers between 0-9
//      >> amount of columnts equals 9
//      >> there is no dots
//      >> each number appears only once in a row
const validateSudoku = (sudokuGrid) => {
    const validateColumns = sudokuGrid.length === 9;
    const onlyNumbs = sudokuGrid.every( row => /^[\x30-\x39]{9}$/.test(row));
    const hasDot = sudokuGrid.every( row => /\./g.test(row));

    console.log(" >> validateColumns:", validateColumns);
    console.log(" >> onlyNumbs:", onlyNumbs);
    console.log(" >> !hasDot:", !hasDot);
    return onlyNumbs && !hasDot && validateColumns;
}
console.log("\n1.2.0 Validating sudoku:".toUpperCase())
console.log("Validation compelete", validateSudoku(sudoku1D));


// 1.3 convert strings into inner arrays
sudoku2D = sudoku1D.map(item => item.split(""));
console.log("\n1.3 CONVERTING SUDOKU TO 2D:");
console.table(sudoku2D);


// 1.3 convert elements to numbers
sudoku2D = sudoku2D.map(innerArr => innerArr.map(element => Number(element)));
console.log("\n1.3 CONVERTING SUDOKU TO 2D:");
console.table(sudoku2D);


// 1.4 find all possible numbers
//      >> 1.4.1 check if there is any matches on current row
//      >> 1.4.2 check if there is any matches on current column
//      >> 1.4.3 check if there is any matches on current grid
const noMatchFound = (sudokuTable, row, col, num) => {
    
    //getting the possition of a grid
    col_pos = Math.floor(col/3)*3;
    row_pos = Math.floor(row/3)*3;
    //console.log("col_pos:", col_pos," row_pos:", row_pos)
    // 1.4.1 check row 
    for (let i = 0; i < 9; i++) {
        //console.log("sudokuTable[col][i]", sudokuTable[col][i], "=== num", num)
        if (sudokuTable[col][i] === num) {
            return false;
        }
    } 
    
    // 1.4.1 check col
    for (let i = 0; i < 9; i++) {
        if (sudokuTable[i][row] === num) {
            return false;
        }
    } 
    
    // 1.4.1 check col
    for (let i = 0; i < 3; i++) {
        for (let j = 0; j < 3; j++) {
            if (sudokuTable[col_pos + i][row_pos + j] === num) {
                return false;
            }
        }
    } 

    // if no matches found we can allow inserting the number
    return true;
}

// validating occurencies of all defaiult numbers, 
// to make sure that no row, column or grid has more 
// than one of the same number.
const validateOccurencies = (sudoku) => {
    let number = 0;
    for (let col = 0; col < 9; col++) {
        for (let row = 0; row < 9; row++) { 
            if (sudoku[col][row] !== 0) {
                console.log("row:", row, "col:", col, "val:",sudoku[col][row])
                number = sudoku[col][row];
                sudoku[col][row] = 0;
                if (!noMatchFound(sudoku, row, col, number)) {
                    sudoku[col][row] = number;
                    return false;
                } else {
                    sudoku[col][row] = number;
                }
            }
        }
    }
    return true;
}
if(!validateOccurencies(sudoku2D)){
    console.log("invalid board, found multiple occurencies of same number. Program is shut down.")
    return false;
}


// iterating through all the cells and inputs a number if possible. 
// Then returns the filled sudoku.
const fillCell = (sudoku) => {
    let newSudoku = sudoku;
    // iterating through all columns
    for (let col = 0; col < 9; col++) {
        //console.log("fillCell:", col)
        // iterating through all rows
        for (let row = 0; row < 9; row++) {

            // Check that current cell doesn't already have a value other than 0
            if(newSudoku[col][row] === 0){
                ///console.log("Found a zero at col =", col, " row =", row);
                for (let num = 1; num < 10; num++) {

                    // if the number is not already inserted in a col, row or a grid then we can insert it.
                    if (noMatchFound(newSudoku, row, col, num)) {
                        newSudoku[col][row] = num;
                        
                        // if all recursive calls succeeded, return the solved board
                        if (fillCell(newSudoku)){
                            return newSudoku;
                        // else set back pervious cell to zero
                        } else {
                            newSudoku[col][row] = 0;
                        } 
                    }
                }
                return false;
            }
        }
    }
    return newSudoku;
}

// if validation is ok, then it wil output the solved sudoku
console.time("solve")
console.table("1.5 Solution:".toUpperCase())

if (validateSudoku(sudoku1D)) {
    console.table(fillCell(sudoku2D))
} else {
    console.log("There is something wrong with the sudoku.")
}

console.timeEnd("solve")